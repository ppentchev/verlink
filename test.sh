#!/bin/sh
#
# Copyright (c) 2016, 2018  Peter Pentchev
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
# OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
# OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
# SUCH DAMAGE.

set -e
[ -z "$TEST_VERBOSE" ] || set -x

create_files()
{
	v="$1"
	mkdir -p -- "$tdir/bin" "$tdir/etc/xyzzy" "$tdir/usr/lib/xyzzy"
	echo "version $v of a shell script" > "$tdir/bin/xyzzy"
	ln -sf -- xyzzy "$tdir/bin/xyzzy-oof"
	echo "version $v of a config file" > "$tdir/etc/xyzzy/xyzzy.conf"
	echo "version $v of something else" > "$tdir/usr/lib/xyzzy/something.dat"

	if [ "$v" = '2.1' ]; then
		echo "version $v of a new script" > "$tdir/bin/xyzzy-whee"
	fi
}

check_template()
{
	local tag="$1"
	shift

	if ! "$@" > "$tempf"; then
		echo "'$tag' failed for $p-$v" 1>&2
		exit 1
	fi
	if ! diff -q -- "$tempexp" "$tempf"; then
		echo "'$tag' did not output the expected result:" 1>&2
		diff -u -- "$tempexp" "$tempf" 1>&2
		exit 1
	fi
}

check_single_line()
{
	local tag="$1"
	local expected="$2"
	shift 2

	printf '%s\n' "$expected" > "$tempexp"
	check_template "$tag" "$@"
}

show_files()
{
	local what="${1-init}"

	printf '\nAfter verlink -p %s -v %s %s:\n' "$p" "$v" "$what"
	find -- "$tdir" \! -type d \! -path "$tdir/usr/lib/verlink/xyzzy/*/*" -print | env LANG=C sort | while read dst; do
		src="$(readlink -- "$dst" || true)"
		if [ -n "$src" ]; then
			echo "$dst -> $src"
		else
			echo "$dst"
		fi
	done
	printf '=======\n\n'
}

# Okay, this is kind of crazy, but the shell's rules for
# short-circuiting and boolean evaluation and precedence are,
# well, kind of crazy, too :)

not_a_link()
{
	[ ! -L "$1" ]
}

is_a_link()
{
	[ -L "$1" ]
}

doesnt_point_to()
{
	[ "$(readlink -- "$1")" != "$2" ]
}

ver_and_link()
{
	[ "$v" = "$1" ] && [ -L "$2" ]
}

ver_and_not_link()
{
	[ "$v" = "$1" ] && [ ! -L "$2" ]
}

check_symlinks()
{
	local tag="$1"

	if not_a_link "$tdir/bin/xyzzy" ||
	   not_a_link "$tdir/bin/xyzzy-oof" ||
	   doesnt_point_to "$tdir/bin/xyzzy-oof" 'xyzzy' ||
	   doesnt_point_to "$tdir/usr/lib/verlink/$p/$v/bin/xyzzy-oof" 'xyzzy' ||
	   ver_and_link '1.0' "$tdir/bin/xyzzy-whee" ] ||
	   ver_and_not_link '2.1' "$tdir/bin/xyzzy-whee" ||
	   not_a_link "$tdir/usr/lib/xyzzy/something.dat" ||
	   not_a_link "$tdir/etc/xyzzy/xyzzy.conf"; then
		echo "'$tag' did not symlink the correct files" 1>&2
		exit 1
	fi
}

check_empty()
{
	local tag="$1"

	find -- "$tdir/" -type d -empty > "$tempf"
	if [ -s "$tempf" ]; then
		echo "'$tag' left empty directories for $p-$v" 1>&2
		cat -- "$tempf" 1>&2
		exit 1
	fi
}

switch_to()
{
	local nv="$1"

	if ! $VERLINK -D "$tdir" -p "$p" -r "$nv" switch; then
		echo "'switch' failed from $p-$v to $p-$nv" 1>&2
		exit 1
	fi
	v="$nv"

	check_single_line 'packages' "$p" $VERLINK -D "$tdir" packages

	check_single_line 'current' "$v" $VERLINK -D "$tdir" -p "$p" current

	printf '%s\n' '1.0' '2.1' > "$tempexp"
	check_template 'available' $VERLINK -D "$tdir" -p "$p" available

	show_files

	check_symlinks "switch to $v"
	check_empty "switch to $v"
}

do_unlink()
{
	if ! $VERLINK -D "$tdir" -p "$p" unlink; then
		echo "'unlink' failed for $p-$v" 1>&2
		exit 1
	fi

	find -- "$tdir" \! -type d \! -path "$tdir/usr/lib/verlink/xyzzy/*" \! -path "$tdir/etc/*" \! -path "$tdir/bin/xyzzy-oof" > "$tempf"
	if [ -s "$tempf" ]; then
		echo "'unlink' left some files over:" 1>&2
		cat -- "$tempf" 1>&2
		exit 1
	fi

	if [ -L "$tdir/usr/lib/verlink/xyzzy/current" ]; then
		echo "'unlink' failed to remove the 'current' symlink" 1>&2
		exit 1
	fi

	check_empty 'unlink'
}

: "${VERLINK:=./verlink.pl}"

tempf="$(mktemp verlink-output.txt.XXXXXX)"
trap "rm -f -- '$tempf'" EXIT HUP INT QUIT TERM
tempexp="$(mktemp verlink-expected.txt.XXXXXX)"
trap "rm -f -- '$tempf' '$tempexp'" EXIT HUP INT QUIT TERM

if [ -z "$TEST_NO_TEMP_DIR" ]; then
	tdir="$(mktemp -d verlink.XXXXXX)"
	if [ "${tdir#/}" = "$tdir" ]; then
		tdir="$(pwd)/$tdir"
	fi
	trap "rm -rf -- '$tempf' '$tempexp' '$tdir'" EXIT HUP INT QUIT TERM
else
	tdir="$(pwd)/test-verlink-dir"
	rm -rf -- "$tdir"
	mkdir -- "$tdir"
fi

p='xyzzy'
vdir="$tdir/usr/lib/verlink"
pdir="$vdir/xyzzy"

create_files '1.0'

echo 'Before the start of the test:'
find -- "$tdir/" -type f -print0 | env LANG=C sort -z | xargs -0 egrep -e '^'
printf '=======\n\n'

if ! $VERLINK -D "$tdir" packages > "$tempf"; then
	echo "'packages' on an empty repository failed" 1>&2
	exit 1
fi
if [ -s "$tempf" ]; then
	echo "'packages' on an empty repository produced non-empty output" 1>&2
	cat -- "$tempf" 1>&2
	exit 1
fi

if ! $VERLINK -D "$tdir" -p "$p" -r "$v" init; then
	echo "'init' failed for $p-$v" 1>&2
	exit 1
fi

if [ ! -d "$pdir" ]; then
	echo "'init' did not create /usr/lib/verlink" 1>&2
	exit 1
elif [ ! -d "$pdir/$v" ]; then
	echo "'init' did not create /usr/lib/verlink/$p/$v" 1>&2
	exit 1
fi

check_single_line 'packages' "$p" $VERLINK -D "$tdir" packages

check_single_line 'current' "$v" $VERLINK -D "$tdir" -p "$p" current

check_single_line 'available' "$v" $VERLINK -D "$tdir" -p "$p" available

show_files

check_symlinks "init $v"

if $VERLINK -D "$tdir" -p "$p" -r "$v" init; then
	echo "'init' succeeded for a second time for $p-$v" 1>&2
	exit 1
fi

do_unlink

create_files '2.1'

if ! $VERLINK -D "$tdir" -p "$p" -r "$v" init; then
	echo "'init' failed for $p-$v" 1>&2
	exit 1
fi

if [ ! -d "$pdir/$v" ]; then
	echo "'init' did not create /usr/lib/verlink/$p/$v" 1>&2
	exit 1
fi

check_single_line 'packages' "$p" $VERLINK -D "$tdir" packages

check_single_line 'current' "$v" $VERLINK -D "$tdir" -p "$p" current

printf '%s\n' '1.0' "$v" > "$tempexp"
check_template 'available' $VERLINK -D "$tdir" -p "$p" available

show_files

check_symlinks "init $v"

switch_to '1.0'

switch_to '2.1'

do_unlink

switch_to '1.0'

# OK, now let's try to handle a weird case that is not really supported,
# but is kinda sorta expected to work in some environments...

later='bin/installed-later'

if [ -f "$tdir/$later" ]; then
	echo "The $tdir/$later file already exists; how come?" 1>&2
	exit 1
fi

echo 'is this really needed?' > "$pdir/$v/bin/installed-later"

if $VERLINK -D "$tdir" -p "$p" -r "$v" switch; then
	echo "'switch' to the same version with a changed directory should not have succeeded" 1>&2
	exit 1
fi

if ! $VERLINK -D "$tdir" -p "$p" -r "$v" -m switch; then
	echo "'switch' to the same version with a changed directory and -m failed" 1>&2
	exit 1
fi

if [ ! -f "$tdir/$later" ]; then
	echo "The $tdir/$later file was not created by the switch" 1>&2
	exit 1
fi
