Change log for verlink
======================

1.2.0 (2018/08/26)
------------------

- Correct two typographical errors.
- Add the --features long option.

1.1.0 (2016/11/14)
------------------

- Add the -m option to ignore missing files in "switch" and "unlink".

1.0.0 (2016/11/06)
------------------

- INCOMPATIBLE: Handle files in /etc, too.
- Check for empty directories after "switch", too, in test.sh.

0.1.1 (2016/11/04)
------------------

- Fix the "unlink" operation when -D is not specified.

0.1.0 (2016/11/03)
------------------

- Initial semi-public release.

Contact: Peter Pentchev <roam@ringlet.net>
