#!/usr/bin/perl
#
# Copyright (c) 2016, 2018  Peter Pentchev
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
# OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
# OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
# SUCH DAMAGE.

use v5.14;
use strict;
use warnings;

use Getopt::Std;
use List::MoreUtils qw(part);
use List::Util qw(any max);
use Path::Tiny;

use constant usage_message => <<'EOUSAGE' =~ s/\n$//r;
Usage:	verlink [-Nv] [-D rootdir] -p package available
	verlink [-Nv] [-D rootdir] -p package current
	verlink [-Nv] [-D rootdir] -p package -r version init
	verlink [-Nv] [-D rootdir] packages
	verlink [-mNv] [-D rootdir] -p package -r version switch
	verlink [-mNv] [-D rootdir] -p package unlink
	verlink -V | -h | --version | --help | --features

	-D	specify the root of the directory hierarchy to operate on
	-h	display program usage information and exit
	-m	ignore missing files on "unlink" and "switch"
	-N	no-operation mode; do not actually collect file info
	-p	specify the name of the package to operate on
	-r	specify the version (release) of the package
	-V	display program version information and exit
	-v	verbose operation; display diagnostic output
EOUSAGE

use constant version_string => '1.2.0';
use constant version_message => 'verlink '.version_string;
use constant features_message => 'Features: verlink='.version_string;

use constant ver_base => '/usr/lib/verlink';

sub ok($)
{
	{ status => 0, msg => $_[0] }
}

sub err($)
{
	{ status => 1, msg => $_[0] }
}

sub usage(;$)
{
	my ($err) = $_[0] // 1;

	$err? err(usage_message): ok(usage_message)
}

sub check_wait_result($ $ $)
{
	my ($stat, $pid, $name) = @_;
	my $sig = $stat & 127;
	if ($sig != 0) {
		die "Program '$name' (pid $pid) was killed by signal $sig\n";
	} else {
		my $code = $stat >> 8;
		if ($code != 0) {
			die "Program '$name' (pid $pid) exited with non-zero status $code\n";
		}
	}
}

sub run_command($ @)
{
	my ($cfg, @cmd) = @_;
	my $name = $cmd[0];

	my $pid = open my $f, '-|';
	if (!defined $pid) {
		die "Could not fork for $name: $!\n";
	} elsif ($pid == 0) {
		$cfg->{debug}->("About to run '@cmd'");
		exec { $name } @cmd;
		die "Could not execute '$name': $!\n";
	}
	my @res = <$f>;
	close $f;
	check_wait_result $?, $pid, $name;
	return @res;
}

sub parse_options($ $ $)
{
	my ($opts, $args, $callback) = @_;

	my %common_options = map { $_ => 0 } qw(D N v);
	my %cmd_options = (
		available => { p => 1 },
		current => { p => 1 },
		init => { p => 1, r => 1 },
		packages => {},
		switch => { m => 0, p => 1, r => 1 },
		unlink => { m => 0, p => 1 },
	);

	my $options_applicable = sub {
		my ($cb) = @_;
		my $cmd = $args->[0];

		!defined $cmd_options{$cmd}?
			$cb->():
			do {
				my $o = $cmd_options{$cmd};
				my @opt_req = grep { $o->{$_} } keys %{$o};
				my @missing_req = sort grep {
					!defined $opts->{$_}
				} @opt_req;
				my @extra = sort grep {
					!defined $o->{$_} &&
					!defined $common_options{$_}
				} keys %{$opts};
				my @msg = (
					(map { err("The '$cmd' command requires the -$_ option") } @missing_req),
					(map { err("The '$cmd' command does not expect the -$_ option") } @extra),
				);

				@msg?
					@msg:
					$cb->()
			}
	};

	my $options_valid = sub {
		my ($cb) = @_;
		my @msg = map {
			defined $opts->{$_} && $opts->{$_} =~ m{/}?
				err("The value of the -$_ option may not contain slashes"):
				()
		} qw(p r);

		@msg?
			@msg:
			$cb->()
	};

	my $check_rootdir = sub {
		my ($dir, $cb) = @_;
		my $path = path($dir);

		$path->is_dir?
			$cb->($path):
			err("Nonexistent root directory '$dir'")
	};

	my $get_rootdir = sub {
		my ($cb) = @_;
		my $dir = $opts->{D};

		$check_rootdir->(
			defined $dir && $dir ne ''? $dir: '/',
			$cb
		)
	};

	@{$args}?
		$options_applicable->(sub {
			$options_valid->(sub {
				$get_rootdir->(sub {
					my ($root) = @_;
				my $vlink = $root->child('usr/lib/verlink');
					my $p_dir = defined $opts->{p}?
						$vlink->child($opts->{p}):
						undef;
					my $r_dir = defined $p_dir && defined $opts->{r}?
						$p_dir->child($opts->{r}):
						undef;

					$callback->({
						debug => $opts->{v}? sub { say STDERR "RDBG $_[0]" }: sub {},
						noop => $opts->{N}? sub { $_[0]->() }: sub { $_[1]->() },

						root => $root,
						verlink => $vlink,

						command => $args->[0],
						arguments => [ @{$args}[1..$#{$args}] ],
						package => $opts->{p},
						version => $opts->{r},

						p_dir => $p_dir,
						r_dir => $r_dir,

						ignore_missing => $opts->{m},
					})
				})
			})
		}):
		usage
}

sub get_options($)
{
	my ($callback) = @_;

	my %opts;
	getopts('hmNp:r:D:Vv-:', \%opts)?
		$callback->(\%opts, \@ARGV):
		usage
}

sub parse_opts_ver_help($ $ $)
{
	my ($opts, $args, $callback) = @_;
	my $has_dash = defined $opts->{'-'};
	my $dash_help = $has_dash && $opts->{'-'} eq 'help';
	my $dash_version = $has_dash && $opts->{'-'} eq 'version';
	my $dash_features = $has_dash && $opts->{'-'} eq 'features';
	my @msg = (
		($opts->{V} || $dash_version? ok(version_message): ()),
		(              $dash_features? ok(features_message): ()),
		($opts->{h} || $dash_help? usage(0): ()),
	);

	$has_dash && !$dash_help && !$dash_version && !$dash_features?
		err("Invalid long option '".$opts->{'-'}."' specified"):
		@msg?
			@msg:
			$callback->($opts, $args)
}

sub display_and_exit(@)
{
	my @res = @_;
	exit 0 unless @res;

	my @by_res = part { $_->{status}? 1: 0 } @res;

	say STDERR $_->{msg} for @{$by_res[1] || []};
	say $_->{msg} for @{$by_res[0] || []};
	exit max map $_->{status}, @res;
}

sub cmd_packages($)
{
	my ($cfg) = @_;
	my $v = $cfg->{verlink};
	my $d;

	$v->is_dir?
		map { ok($_->basename) } grep { $_->is_dir } $v->children:
		$v->exists?
			err("The verlink base '$v' is not a directory"):
			()
}

sub cmd_available($)
{
	my ($cfg) = @_;
	my $p = $cfg->{p_dir};

	$p->is_dir?
		map { ok($_) } sort map { $_->basename } grep { $_->is_dir && ! -l $_ } $p->children:
		$p->exists?
			err("The '$cfg->{package} package is not managed by verlink"):
			()
}

sub validate_current_version($ $)
{
	my ($cfg, $callback) = @_;
	my $p_dir = $cfg->{p_dir};
	my $c_path = $p_dir->child('current');

	$p_dir->is_dir?
		-l "$c_path"?
			do {
				my $current = readlink $c_path;

				$current !~ m{/}?
					$c_path->is_dir?
						$callback->($p_dir->child($current)):
						err("The $cfg->{package} package has a nonexistent current version '$current' recorded"):
					err("The $cfg->{package} package has an invalid current version '$current' recorded")
			}:
			$callback->(undef):
		err("The $cfg->{package} package is not managed by verlink")
}

sub cmd_current($)
{
	my ($cfg) = @_;
	my $p_dir = $cfg->{p_dir};

	validate_current_version($cfg, sub {
		my ($r_dir) = @_;

		defined $r_dir?
			ok($r_dir->basename):
			()
	})
}

sub c_try($ $; $)
{
	my ($run, $cb, $catch) = @_;

	eval {
		$run->();
		1
	}?
		$cb->():
		do {
			my $msg = $@ =~ s/\n$//r;

			defined $catch?
				$catch->($msg):
				err($msg)
		}
}

sub walk_and_collect($ $ $);
sub walk_and_collect($ $ $)
{
	my ($d, $level, $where) = @_;
	my @by_type = part { -d? 0: -l? 3: -f? 1: 2 } $d->children;

	my $check_skip = sub {
		my ($dir, $cb) = @_;
		my $name = $dir->basename;
		my $skip = $level == 0?
			$name =~ /^ dev | lost\+found | media | mnt | run | srv | sys $/x:
			$level == 2?
				$where eq 'usr/lib/' && $name eq 'verlink':
				0;

		$skip?
			():
			$cb->($dir)
	};

	defined $by_type[2]?
		map {
			"Neither a directory nor a regular file: $_"
		} @{$by_type[2]}:
		(
			# Directories
		 	(
				map {
					$check_skip->($_, sub {
						walk_and_collect(
							$_[0],
							$level + 1,
							$where . $_[0]->basename . '/'
						)
					})
				} @{$by_type[0] // []}
			),

			# Files
			(
				map {
					{
						where => $where,
						name => $_->basename,
						prefix => ('../' x $level) =~ s{/$}{}r,
					}
				} @{$by_type[1] // []}
			),

			# Symlinks
			(
				map {
					{
						where => $where,
						name => $_->basename,
						target => readlink($_),
						prefix => ('../' x $level) =~ s{/$}{}r,
					}
				} @{$by_type[3] // []}
			),
		)
}

sub do_mkdir($ $ $) {
	my ($cfg, $dir, $cb) = @_;

	$dir->is_dir?
		$cb->():
		$cfg->{noop}->(sub {
			(
			ok("mkdir -p -- '$dir'"),
			$cb->(),
			)
		}, sub {
			c_try(sub {
				$dir->mkpath
			}, $cb, sub {
				err("Could not create the '$dir' directory: $_[0]")
			})
		})
}

sub do_symlink($ $ $ $)
{
	my ($cfg, $src, $dst, $cb) = @_;
	my $p_dst = ref $dst eq ''? path($dst): $dst;

	do_mkdir($cfg, $p_dst->parent, sub {
		$cfg->{noop}->(sub {
			(
			ok("ln -s -- '$src' '$dst'"),
			$cb->(),
			)
		}, sub {
			symlink($src, $dst)?
				$cb->():
				err("Could not symlink '$src' to '$dst': $!")
		})
	})
}

sub cmd_init($)
{
	my ($cfg) = @_;
	my ($root, $r_dir) = ($cfg->{root}, $cfg->{r_dir});

	my $do_rename = sub {
		my ($from, $to, $cb) = @_;

		$cfg->{noop}->(sub {
			(
			ok("mv -- '$from' '$to'"),
			$cb->(),
			)
		}, sub {
			c_try(sub {
				$from->move($to)
			}, $cb, sub {
				err("Could not rename '$from' to '$to': $_[0]")
			})
		})
	};

	my $do_move = sub {
		my ($data) = @_;
		my $tdir = $r_dir->child($data->{where});

		$cfg->{debug}->("Creating $tdir") unless $tdir->is_dir;
		do_mkdir($cfg, $tdir, sub {
			my $full = "$data->{where}$data->{name}";
			my $from = $root->child($full);
			my $to = $r_dir->child($full);

			defined $data->{target}?
				do {
					$cfg->{debug}->("Recoring a symlink from $data->{target} to $full");
					do_symlink($cfg, $data->{target}, $to, sub { () })
				}:
				do {
					$cfg->{debug}->("Moving $from to $to");
					$do_rename->($from, $to, sub {
						do_symlink(
							$cfg,
							$data->{prefix}.ver_base."/$cfg->{package}/$cfg->{version}/$data->{where}$data->{name}",
							$from,
							sub { () }
						)
					})
				}
		})
	};

	$r_dir->is_dir?
		err("Version '$cfg->{version}' of the '$cfg->{package}' package is already managed by verlink"):
		do_mkdir($cfg, $r_dir, sub {
			my @dirs = walk_and_collect($root, 0, '');
			my @errors = grep { ref $_ eq '' } @dirs;

			@errors?
				map { err($_) } @errors:
				do {
					my @res = map { $do_move->($_) } @dirs;

					(
						@res,
						(any { $_->{status} != 0 } @res)?
							():
							do_symlink(
								$cfg,
								$r_dir->basename,
								$cfg->{p_dir}->child('current'),
								sub {
									()
								}
							)
					)
				}
		})
}

sub validate_unlink_real($ $ $)
{
	my ($cfg, $dir, $callback) = @_;

	my @to_unlink = walk_and_collect($dir, 0, '');
	my @errs = grep { ref $_ eq '' } @to_unlink;

	@errs?
		@errs:
		do {
			my $wanted = $cfg->{ignore_missing}? sub { -e }: sub { 1 };
			my @links = grep { $wanted->($_) }
				map { $cfg->{root}->child("$_->{where}$_->{name}") }
				@to_unlink;
			my @not_links = grep { ! -l } @links;

			@not_links?
				map { err("$_ is not a symlink") } @not_links:
				do {
					my %links_to = map { $_ => path(readlink) } @links;
# Argh, relative symlinks!
#					my @outside = sort grep {
#						!$dir->subsumes($links_to{$_})
#					} keys %links_to;
					my @outside = ();
					@outside?
						map {
							err("$_ is not a link within $dir: $links_to{$_}")
						} @outside:
						$callback->(\@links)
				}
		}
}

sub validate_unlink($ $ $)
{
	my ($cfg, $dir, $callback) = @_;

	defined $dir?
		validate_unlink_real($cfg, $dir, $callback):
		$callback->([])
}

sub do_unlink($ $ $)
{
	my ($cfg, $path, $cb) = @_;

	$cfg->{noop}->(sub {
		(
		ok("rm -- '$path'"),
		$cb->(),
		)
	}, sub {
		c_try(sub {
			$path->remove
		}, $cb, sub {
			err("Could not remove '$path': $_[0]")
		})
	})
}

sub do_rmdir($ $ $)
{
	my ($cfg, $dir, $cb) = @_;

	$cfg->{noop}->(sub {
		(
		ok("rmdir -- '$dir'"),
		$cb->(),
		)
	}, sub {
		rmdir($dir)?
			$cb->():
			err("Could not remove the '$dir' directory: $!")
	})
}

sub cmd_unlink($)
{
	my ($cfg) = @_;
	my ($root, $p_dir) = ($cfg->{root}, $cfg->{p_dir});

	my $dirs_will_be_empty = sub {
		my ($files, $dirs_hash) = @_;

		my $find_empty;
		$find_empty = sub {
			my ($files, $dirs, $empty) = @_;

			my $check_dir_empty;
			$check_dir_empty = sub {
				my ($dir, $c_empty) = @_;

				$root->subsumes($dir)?
					do {
						my @children = $dir->children;
						my @not_our_files = grep { !$files->{$_} } @children;
						my @not_empty_dirs = grep { !$c_empty->{$_} } @not_our_files;

						my $new_empty = @not_empty_dirs?
							$c_empty:
							{
								%{$c_empty},
								$dir => 1,
							};

						my $parent = $dir->parent;
						$parent eq '' || $parent eq $dir?
							$new_empty:
							$check_dir_empty->($dir->parent, $new_empty);
					}:
					$c_empty
			};

			@{$dirs}?
				do {
					my ($first, @rest) = @{$dirs};

					$find_empty->(
						$files,
						\@rest,
						$check_dir_empty->(path($first), $empty)
					)
				}:
				reverse sort keys %{$empty}
		};

		$find_empty->($files, [sort { -(length($a) <=> length($b)) } keys %{$dirs_hash}], {})
	};

	validate_current_version($cfg, sub {
		my ($r_dir) = @_;

		validate_unlink($cfg, $r_dir, sub {
			my ($links) = @_;
			my %files = map { $_ => 1 } @{$links};
			my %dirs = map { $_->parent => 1 } @{$links};
			my @to_remove = $dirs_will_be_empty->(\%files, \%dirs);
			$cfg->{debug}->("unlink: about to remove ".scalar(@{$links})." symlinks and ".scalar(@to_remove)." directories");
			my @res = (
				(
					map {
						do_unlink($cfg, $_, sub { () })
					} @{$links}
				),
				(
					map {
						do_rmdir($cfg, $_, sub { () })
					} @to_remove
				),
			);

			(
				@res,
				(any { $_->{status} } @res)?
					():
					do_unlink($cfg, $p_dir->child('current'), sub { () })
			)
		})
	})
}

sub cmd_switch($)
{
	my ($cfg) = @_;
	my $r_dir = $cfg->{r_dir};

	validate_current_version($cfg, sub {
		my ($c_dir) = @_;

		$r_dir->is_dir?
			validate_unlink($cfg, $c_dir, sub {
				my ($c_links) = @_;
				my @n_links = walk_and_collect($r_dir, 0, '');
				my @errs = grep { ref $_ eq '' } @n_links;

				@errs?
					@errs:
					do {
						my $root = $cfg->{root};
						my %links = map {
							my $path = "$_->{where}$_->{name}";

							$root->child($path) =>
								$_->{target} // $_->{prefix}.ver_base."/$cfg->{package}/$cfg->{version}/$path"
						} @n_links;
						my @not_links = sort grep { -e && ! -l } keys %links;
						@not_links?
							map { err("$_ exists and is not a symlink") } @not_links:
							do {
								my $current = $cfg->{p_dir}->child('current');
								my @res_unlink = map {
									do_unlink($cfg, $_, sub { () })
								} (@{$c_links}, $current);

								(
									@res_unlink,
									(any { $_->{status} } @res_unlink)?
										():
										do {
											my @res_link = map {
												do_symlink($cfg, $links{$_}, $_, sub { () })
											} sort keys %links;

											(
												@res_link,
												(any { $_->{status} } @res_link)?
													():
													do_symlink($cfg, $cfg->{version}, $current, sub { () })
											)
										}
								)
							}
					}
			}):
			err("The $cfg->{package} package does not have a $cfg->{version} version")
	})
}

MAIN:
{
	display_and_exit get_options sub {
		# opts, args -- output
		parse_opts_ver_help $_[0], $_[1], sub {
			# opts, args -- output
			parse_options $_[0], $_[1], sub {
				# cfg -- output
				my ($cfg) = @_;

				my %cmds = (
					available => \&cmd_available,
					current => \&cmd_current,
					init => \&cmd_init,
					packages => \&cmd_packages,
					switch => \&cmd_switch,
					unlink => \&cmd_unlink,
				);

				my $cmd = $cfg->{command};
				my $handler = $cmds{$cmd};
				defined $handler?
					$handler->($cfg):
					(
						err("Unrecognized command '$cmd'"),
						usage,
					)
			}
		}
	};
}
