verlink - the simultaneously-installed package versions manager
===============================================================

The verlink tool is a simple manager for several versions of a package
installed at the same time.  It comes in useful when a package is not
necessarily completely ready for operation after installing it, when
a restart needs to be performed at a later date, or in other cases.


Querying verlink's database of currently installed packages
===========================================================

To see what packages are currently managed by verlink at all, run:

    verlink -D /root/path packages

To see what versions are available for a specific package:

    verlink -D /root/path -p pkgname available

To see which one of them is currently active (i.e. has symlinks from
the "real" file locations to verlink's copy of the package):

    verlink -D /root/path -p pkgname current

All of the commands in the following sections may be run with the -N
command-line switch for no-operation mode; verlink will display
a series of shell commands that should reproduce its actions if
the same verlink command line was invoked without the -N option.

Recording an installed version of a package
===========================================

The verlink tool assumes that all files within the specified root
directory belong to a package, with the exception of the contents of
the /etc and /usr/lib/verlink directories.  Thus, to add a package
version to its database (and replace the package files with symlinks),
run:

    verlink -D /root/path -p pkgname -r version init

This will create the /root/path/usr/lib/verlink/pkgname/version
directory, move all files under /root/path there, replace them with
symlinks to their new locations, and then note that this version is
currently installed.


Switching to a different version of a package
=============================================

If a /usr/lib/verlink/pkgname/version/ directory has been installed by
external means (e.g. OS packaging), to activate it, run:

    verlink -D /root/path -p pkgname -r version switch
